﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Editor Params
    [Header("SCREENS")]
    public GameObject Debug;
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;

    [Header("DEBUG")]
    public Text txtMapOrder;
    #endregion

    #region Params
    public static UIManager Instance { get; private set; }
    #endregion

    #region Properties
    #endregion

    #region Events
    #endregion

    #region Methods

    private void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 300;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    private void Start()
    {
        if (Application.isEditor)
        {
            Debug.SetActive(true);
        }
        else
        {
            Debug.SetActive(false);
        }
    }

    public void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }
    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }
    public void OnWinGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);
    }
    public void OnLoseGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
    }

    public void UpdateDebugMap(int value)
    {
        txtMapOrder.text = "MAP: " + value.ToString();
    }

    #endregion
}