﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Theme
{
    public Color nameColor;
    public Color tileColor;
    public Texture2D pattern;
}

public class ThemeManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public List<Theme> themesList;

    [Header("Elements")]
    public Text txtName;
    public Material tileMaterial;
    public Material groundMaterial;
    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static ThemeManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    #endregion

    #region DEBUG
    #endregion

}
