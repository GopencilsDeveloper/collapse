﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public List<Texture2D> texturesList;
    #endregion

    #region PARAMS
    public static MapManager Instance { get; private set; }
    public int index = -1;
    public Texture2D currentTexture;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        index = DataManager.Instance.currentMapOrder;
    }

    #endregion

    #region DEBUG

    public void Next()
    {
        index++;
        index = index > (texturesList.Count - 1) ? 0 : index;
        currentTexture = texturesList[index];
        Board.Instance.Spawn(currentTexture);
        UIManager.Instance.UpdateDebugMap(index);
        DataManager.Instance.SaveOrder(index);
    }

    public void Previous()
    {
        index--;
        index = index < 0 ? (texturesList.Count - 1) : index;
        currentTexture = texturesList[index];
        Board.Instance.Spawn(currentTexture);
        UIManager.Instance.UpdateDebugMap(index);
        DataManager.Instance.SaveOrder(index);
    }

    #endregion

}
