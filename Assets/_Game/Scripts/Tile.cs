﻿using System.Collections;
using UnityEngine;
using System.Linq;

public class Tile : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Renderer rend;
    public Collider col;
    #endregion

    #region PARAMS
    private MaterialPropertyBlock propBlock;
    public bool isFolding;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        propBlock = new MaterialPropertyBlock();
        isFolding = false;
    }

    private void OnEnable()
    {
        col.enabled = true;
    }

    private void OnDisable()
    {
        isFolding = false;
    }

    public void SetColor(Color inputColor)
    {
        rend.GetPropertyBlock(propBlock);
        propBlock.SetColor("_Color", inputColor);
        rend.SetPropertyBlock(propBlock);
    }

    public void Hide()
    {
        col.enabled = false;
        SetColor(Color.clear);
    }

    public void Show()
    {
        col.enabled = true;
        SetColor(Color.yellow);
    }

    public void EnableCollider()
    {
        col.enabled = true;
    }

    public void DisableCollider()
    {
        col.enabled = false;
    }

    // public void RotateAround(Vector3 point, Vector3 axis, float angle, float inTimeSecs)
    // {
    //     if (!gameObject.activeSelf)
    //     {
    //         return;
    //     }
    //     StartCoroutine(C_RotateAround(point, axis, angle, inTimeSecs));
    // }

    // IEnumerator C_RotateAround(Vector3 point, Vector3 axis, float angle, float inTimeSecs)
    // {
    //     isFolding = true;
    //     Board.Instance.isFolding = true;
    //     float currentTime = 0.0f;
    //     float angleDelta = angle / inTimeSecs; //how many degress to rotate in one second
    //     float ourTimeDelta = 0;
    //     while (currentTime < inTimeSecs)
    //     {
    //         currentTime += Time.deltaTime;
    //         ourTimeDelta = Time.deltaTime;

    //         //Make sure we dont spin past the angle we want.
    //         if (currentTime > inTimeSecs)
    //             ourTimeDelta -= (currentTime - inTimeSecs);

    //         gameObject.transform.RotateAround(point, axis, angleDelta * ourTimeDelta);
    //         yield return null;
    //     }

    //     isFolding = false;
    //     Board.Instance.isFolding = false;
    // }

    private void OnTriggerEnter(Collider other)
    {
        if (isFolding)
        {
            if (other.gameObject.CompareTag("Tile"))
            {
                // Tile tile = other.gameObject.GetComponent<Tile>();
                // tile.Hide();
                print("RIGHT!");
            }
            else
            if (other.gameObject.CompareTag("EmptyTile") && other.gameObject.GetComponent<EmptyTile>().canCollide)
            {
                SetColor(Color.red);
                Board.Instance.isFoldRight = false;
                print("WRONG!");
            }
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
