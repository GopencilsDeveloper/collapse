﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region Editor Params
    #endregion

    #region Params
    private static GameManager instance;
    public GameState currentState;
    #endregion

    #region Properties
    public static GameManager Instance { get => instance; private set => instance = value; }
    #endregion

    #region Events
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region Methods

    private void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
    }

    public void PlayGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
