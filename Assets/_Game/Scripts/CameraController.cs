﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    public void SetCamera(Vector3 pos)
    {
        Camera.main.transform.position = pos;
    }
}
