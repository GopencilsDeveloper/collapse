﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public float floatDuration;
    public Transform tilesParent;
    public Transform emptyTilesParent;
    public Transform axisX;
    public Transform axisZ;
    public Transform pivot;
    public GameObject tempGO;
    #endregion

    #region PARAMS
    public static Board Instance { get; private set; }
    public int currentSizeX;
    public int currentSizeZ;
    public List<Tile> tilesList = new List<Tile>();
    public List<EmptyTile> emptyTilesGround = new List<EmptyTile>();
    public bool isFolding;
    public Vector3 pivotPos;
    public List<Tile> tilesFoldList = new List<Tile>();
    Swipe lastSwipeDirection;
    public bool isFoldRight;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    public event System.Action OnRotated;
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    void RegisterEvents()
    {
        SwipeManager.OnSwipeDetected += OnSwipeDetectedHandler;
        OnRotated += OnRotatedHandler;
    }

    void UnregisterEvents()
    {
        SwipeManager.OnSwipeDetected -= OnSwipeDetectedHandler;
        OnRotated -= OnRotatedHandler;
    }

    private void Start()
    {
        if (Application.isEditor)
        {
            axisX.gameObject.SetActive(true);
            axisZ.gameObject.SetActive(true);
        }
        else
        {
            axisX.gameObject.SetActive(false);
            axisZ.gameObject.SetActive(false);
        }
    }

    void UpdateCurrentSize(int sizeX, int sizeZ)
    {
        currentSizeX = sizeX;
        currentSizeZ = sizeZ;
    }

    private void OnSwipeDetectedHandler(Swipe swipeDirection, Vector2 swipeVelocity)
    {
        if (isFolding)
            return;

        Fold(swipeDirection);
    }

    public void Spawn(Texture2D texture)
    {
        isFoldRight = true;
        isFolding = false;
        ReclaimAllTiles();
        ReclaimAllEmptyTiles();

        UpdateCurrentSize(texture.width, texture.height);

        float cameraRatio = texture.width * 0.1f >= texture.height * 0.1f ? texture.width * 0.1f : texture.height * 0.1f;
        CameraController.Instance.SetCamera(new Vector3(0f, 12f * (1 + cameraRatio), 0f));

        for (int i = -15; i < 15; i++)
        {
            for (int j = -15; j < 15; j++)
            {
                EmptyTile emptyTile = EmptyTilePool.Instance.GetFromPool();
                emptyTile.transform.SetParent(emptyTilesParent);
                emptyTile.transform.localPosition = new Vector3(i - 0.5f, 0, j - 0.5f);
                emptyTilesGround.Add(emptyTile);
            }
        }

        for (int i = 0; i < texture.width; i++)
        {
            for (int j = 0; j < texture.height; j++)
            {

                Color pixelColor = texture.GetPixel(i, j);
                if (pixelColor.a == 0)
                {
                    return;
                }

                Tile tile = TilePool.Instance.GetFromPool();
                tile.transform.SetParent(tilesParent);
                tile.transform.localPosition = new Vector3(i, 0, j);
                tilesList.Add(tile);

                if (pixelColor != Color.white)
                {
                    tile.SetColor(Color.yellow);
                    tile.EnableCollider();
                }
                else
                {
                    tile.SetColor(Color.clear);
                    tile.DisableCollider();
                }
            }
        }

        UpdatePivot();
        UpdateBoardPos();
    }

    private void Fold(Swipe swipeDirection)
    {
        tempGO.transform.position = Vector3.zero;
        tilesFoldList.Clear();
        lastSwipeDirection = swipeDirection;

        switch (swipeDirection)
        {
            case Swipe.Up:

                if (currentSizeZ <= 0)
                    return;

                for (int i = 0; i < tilesList.Count; i++)   //Up
                {
                    Tile tile = tilesList[i];
                    Vector3 pos = tile.transform.localPosition;
                    if (pos.z <= pivotPos.z)
                    {
                        tilesFoldList.Add(tile);
                        tile.isFolding = true;
                        tile.transform.SetParent(tempGO.transform);
                    }
                }
                currentSizeZ = (int)(currentSizeZ * 0.5f) + currentSizeZ % 2;
                RotateAround(tempGO, pivot.transform.position, Vector3.right, 180f, floatDuration, OnRotated);

                break;

            case Swipe.Down:

                if (currentSizeZ <= 0)
                    return;

                for (int i = 0; i < tilesList.Count; i++)   //Down
                {
                    Tile tile = tilesList[i];
                    Vector3 pos = tile.transform.localPosition;
                    if (pos.z >= pivotPos.z)
                    {
                        tilesFoldList.Add(tile);
                        tile.isFolding = true;
                        tile.transform.SetParent(tempGO.transform);
                    }
                }
                currentSizeZ = (int)(currentSizeZ * 0.5f) + currentSizeZ % 2;
                RotateAround(tempGO, pivot.transform.position, Vector3.left, 180f, floatDuration, OnRotated);
                break;

            case Swipe.Left:

                if (currentSizeX <= 0)
                    return;

                for (int i = 0; i < tilesList.Count; i++)   //Left
                {
                    Tile tile = tilesList[i];
                    Vector3 pos = tile.transform.localPosition;
                    if (pos.x >= pivotPos.x)
                    {
                        tilesFoldList.Add(tile);
                        tile.isFolding = true;
                        tile.transform.SetParent(tempGO.transform);
                    }
                }
                currentSizeX = (int)(currentSizeX * 0.5f) + currentSizeX % 2;
                RotateAround(tempGO, pivot.transform.position, Vector3.forward, 180f, floatDuration, OnRotated);
                break;

            case Swipe.Right:

                if (currentSizeX <= 0)
                    return;

                for (int i = 0; i < tilesList.Count; i++)   //Right
                {
                    Tile tile = tilesList[i];
                    Vector3 pos = tile.transform.localPosition;
                    if (pos.x <= pivotPos.x)
                    {
                        tilesFoldList.Add(tile);
                        tile.isFolding = true;
                        tile.transform.SetParent(tempGO.transform);
                    }
                }
                currentSizeX = (int)(currentSizeX * 0.5f) + currentSizeX % 2;
                RotateAround(tempGO, pivot.transform.position, Vector3.back, 180f, floatDuration, OnRotated);
                break;

            default:
                break;
        }

        if (currentSizeX == 1 && currentSizeZ == 1)
        {
            print("WIN!");
        }

    }

    private void OnRotatedHandler()
    {
        if (isFoldRight)
        {
            foreach (Tile tile in tilesFoldList)
            {
                tile.isFolding = false;
                tile.transform.SetParent(tilesParent);
            }
            UpdatePivot();
        }
        else
        {
            
        }
    }

    public void Unfold(Swipe lastSwipeDirection)
    {
        print("Unfold");
        switch (lastSwipeDirection)
        {
            case Swipe.Up:
                RotateAround(tempGO, pivot.transform.position, Vector3.left, 180f, floatDuration, null);
                break;

            case Swipe.Down:
                break;

            case Swipe.Left:
                break;

            case Swipe.Right:
                break;

            default:
                break;
        }
    }



    public void UpdatePivot()
    {
        if (tilesList.Count == 0)
            return;

        float totalX = 0;
        float totalZ = 0;

        for (int i = 0; i < tilesList.Count; i++)
        {
            totalX += tilesList[i].transform.localPosition.x;
            totalZ += tilesList[i].transform.localPosition.z;
        }

        pivotPos = new Vector3((int)(totalX / tilesList.Count) + 0.5f, 0f, (int)(totalZ / tilesList.Count) + 0.5f);
        pivot.transform.localPosition = pivotPos;

        UpdateAxisesPos();
        print("UpdatePivot");
    }

    void UpdateAxisesPos()
    {
        axisX.localPosition = Vector3.right * pivotPos.x;
        axisZ.localPosition = Vector3.forward * pivotPos.z;
    }

    void UpdateBoardPos()
    {
        transform.localPosition = new Vector3(-pivotPos.x, 0f, -pivotPos.z);
    }

    private void ReclaimAllTiles()
    {
        for (int i = 0; i < tilesList.Count; i++)
        {
            TilePool.Instance.ReturnToPool(tilesList[i]);
        }
        tilesList.Clear();
    }

    private void ReclaimAllEmptyTiles()
    {
        for (int i = 0; i < emptyTilesGround.Count; i++)
        {
            EmptyTilePool.Instance.ReturnToPool(emptyTilesGround[i]);
        }
        emptyTilesGround.Clear();
    }

    #endregion

    #region DEBUG

    public void RotateAround(GameObject gameObject, Vector3 point, Vector3 axis, float angle, float inTimeSecs, System.Action OnRotated)
    {
        StartCoroutine(C_RotateAround(gameObject, point, axis, angle, inTimeSecs, OnRotated));
    }

    IEnumerator C_RotateAround(GameObject gameObject, Vector3 point, Vector3 axis, float angle, float inTimeSecs, System.Action OnRotated)
    {
        float currentTime = 0.0f;
        float angleDelta = angle / inTimeSecs; //how many degress to rotate in one second
        float ourTimeDelta = 0;
        while (currentTime < inTimeSecs)
        {
            currentTime += Time.deltaTime;
            ourTimeDelta = Time.deltaTime;

            //Make sure we dont spin past the angle we want.
            if (currentTime > inTimeSecs)
                ourTimeDelta -= (currentTime - inTimeSecs);

            gameObject.transform.RotateAround(point, axis, angleDelta * ourTimeDelta);
            yield return null;
        }

        if (OnRotated != null)
        {
            OnRotated();
        }
    }

    #endregion

}
