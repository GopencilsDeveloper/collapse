﻿using UnityEngine;

public class EmptyTile : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    public Collider col;
    #endregion

    #region PARAMS
    public bool canCollide;
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        canCollide = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tile") && !other.gameObject.GetComponent<Tile>().isFolding)
        {
            canCollide = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Tile") && other.gameObject.GetComponent<Tile>().isFolding)
        {
            canCollide = true;
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
